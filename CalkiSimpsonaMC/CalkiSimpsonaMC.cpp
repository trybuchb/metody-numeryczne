#include <iostream>
#include <string>
#include <fstream>
#include <ctime>
#include <cstdlib>

using namespace std;

string fileName = "load.csv";

double* load();
double liczWielomian(double* tab, double x);

int main() {

	srand(time(NULL));

	double* tab = load();

	double h = (tab[2] - tab[1]) / 4;
	double I = 4 / 2;

	double* Ix = new double[I];

	int licznik = 0;
	for (size_t i = tab[1]; i < tab[2]; i+=(2*h))
	{
		double P = i;
		double K = i + (2 * h);
		double mid = (P + K) / 2;
		Ix[licznik] = (liczWielomian(tab, P) + 4 * liczWielomian(tab,mid) + liczWielomian(tab,K)) * (h / 3);
		licznik++;
	}

	cout << "P: " << I << endl;
	cout << "H: " << h << endl;

	double suma = 0;

	for (size_t i = 0; i < I; i++)
	{
		suma += Ix[i];
	}

	double* Xx = new double[tab[3]];

	double wynik = 0;

	for (size_t i = 0; i < tab[3]; i++)
	{
		int przedzial = (tab[2] - tab[1])* 10 ;
		//cout << "Przedail: " << przedzial << endl;
		Xx[i] = (rand() % (przedzial+10) )/ 10.0;
		Xx[i] = liczWielomian(tab, Xx[i]) / tab[3];
		//cout << Xx[i] << endl;
		wynik += Xx[i];
	}
	//cout << "Wynik: " << wynik << endl;
	wynik = wynik * abs(tab[2] - tab[1]);

	cout << "Wynik Simpson: " << suma << endl;
	cout << "Wynik MC: " << wynik << endl;

	system("PAUSE");
}

double* load() {

	ifstream loadFile;
	loadFile.open(fileName);

	if (!loadFile.is_open()) {
		cout << "Error loading" << endl;
		return false;
	};

	cout << "Wczytane watrosci: " << endl;

	//        [0]          [1]  [2] [3]                [4+]
	// stopien wielomianu, Xp,  Xk,  n,    wartosci przy x dla wielomianu

	int X = 4;

	string stopien;
	string p;
	string k;
	string N;
	//string lPrzedzialow;

	getline(loadFile, stopien, ',');

	int st = stoi(stopien);
	double* tab = new double[st + X];

	getline(loadFile, p, ',');
	getline(loadFile, k, ',');
	getline(loadFile, N, ',');
	//getline(loadFile, lPrzedzialow, ',');

	tab[0] = stod(stopien);
	tab[1] = stod(p);
	tab[2] = stod(k);
	tab[3] = stod(N);
	//tab[4] = stod(lPrzedzialow);

	string temp;
	for (size_t i = X; i < st + X; i++)
	{
		getline(loadFile, temp, ',');
		tab[i] = stod(temp);
	}

	getline(loadFile, temp, '\n');
	tab[st + X] = stod(temp);

	cout << "Stopien:  " << tab[0] << endl;
	cout << "P:  " << tab[1] << endl;
	cout << "K:  " << tab[2] << endl;
	cout << "N:  " << tab[3] << endl;
	//cout << "L:  " << tab[4] << endl;

	for (size_t i = X; i < st + X + 1; i++)
	{
		cout << "X^" << i - X << ":  " << tab[i] << endl;
	}



	cout << endl << "-------------------------------------------------------------------------------------" << endl << endl;

	loadFile.close();

	return tab;
}

double liczWielomian(double* tab, double x) {

	double wynik = 0;

	for (size_t i = 0; i <= tab[0]; i++)
	{
		wynik += tab[i + 4] * pow(x, i);
	}

	return wynik;
}