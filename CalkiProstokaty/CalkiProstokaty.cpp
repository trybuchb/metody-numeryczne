#include <iostream>
#include <string>
#include <fstream>

using namespace std;

string fileName = "load.csv";

double* load();
double liczWielomian(double* tab, double x);

int main() {

	double* tab = load();

	double dx = (tab[2] - tab[1]) / tab[3];

	cout << "dx:  " << dx << endl;
	
	double suma = 0;

	for (double i = 1; i <= tab[3]; i++)
	{
		suma += liczWielomian(tab, tab[1] + i * dx);
	}

	suma *= dx;
	cout << "Wynik:  " << suma << endl;

	system("PAUSE");
}

double* load() {

	ifstream loadFile;
	loadFile.open(fileName);

	if (!loadFile.is_open()) {
		cout << "Error loading" << endl;
		return false;
	};

	cout << "Wczytane watrosci: " << endl;

	//        [0]          [1]  [2] [3]           [4+]
	// stopien wielomianu, Xp,  Xk,  n,   wartosci przy x dla wielomianu

		string stopien;
		string p;
		string k;
		string N;

		getline(loadFile, stopien, ',');

		int st = stoi(stopien);
		double* tab = new double[st + 4];

		getline(loadFile, p, ',');
		getline(loadFile, k, ',');
		getline(loadFile, N, ',');

		tab[0] = stod(stopien);
		tab[1] = stod(p);
		tab[2] = stod(k);
		tab[3] = stod(N);

		string temp;
		for (size_t i = 4; i < st+4; i++)
		{
			getline(loadFile, temp, ',');
			tab[i] = stod(temp);
		}

		getline(loadFile, temp, '\n');
		tab[st+4] = stod(temp);	

		cout << "Stopien:  " << tab[0] << endl;
		cout << "P:  " << tab[1] << endl;
		cout << "K:  " << tab[2] << endl;
		cout << "N:  " << tab[3] << endl;

		for (size_t i = 4; i < st+5; i++)
		{
			cout <<"X^"<< i-4 << ":  " << tab[i] << endl;
		}



	cout << endl << "-------------------------------------------------------------------------------------" << endl << endl;

	loadFile.close();

	return tab;
}

double liczWielomian(double* tab, double x){

	double wynik = 0;

	for (size_t i = 0; i <= tab[0]; i++)
	{
		wynik += tab[i + 4] * pow(x, i);
	}

	return wynik;
}