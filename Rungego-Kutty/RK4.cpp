#include <iostream>
#include <string>
#include <fstream>

using namespace std;

string fileName = "load.csv";

double* load();
double licz(double x, double y);

int main() {
	//double* tab = load();

	double* tab = load();

	double x0 = tab[0];
	double y0 = tab[2];
	double b = tab[1];
	double h = tab[3];
	double N = (b - x0) / h;

	double* tabk = new double[4];
	double* tabx = new double[N+1];
	double* taby = new double[N+1];

	taby[0] = y0;

	tabx[0] = x0;

	for (size_t i = 1; i < N+1; i++)
	{
		tabx[i] = tabx[i-1] + h;
	}


	for (size_t i = 0; i <= N; i++)
	{
		tabk[0] = h * licz(tabx[i], taby[i]);
		tabk[1] = h * licz(tabx[i] + 0.5 * h, taby[i] + 0.5*tabk[0]);
		tabk[2] = h * licz(tabx[i] + 0.5 * h, taby[i] + 0.5*tabk[1]);
		tabk[3] = h * licz(tabx[i] + h, taby[i] + tabk[2]);

		taby[i + 1] = taby[i] + (tabk[0] + 2.0 * tabk[1] + 2.0 * tabk[2] + tabk[3]) / 6.0;
	}

	for (size_t i = 0; i <= N; i++)
	{
		cout << "X: " << tabx[i] << endl;
		cout << "Y: " << taby[i] << endl << endl;
	}

	system("PAUSE");
}

double* load() {

	ifstream loadFile;
	loadFile.open(fileName);

	if (!loadFile.is_open()) {
		cout << "Error loading" << endl;
		return false;
	};

	cout << "Wczytane watrosci: " << endl;

	//        [0]          [1]  [2] [3]           [4+]
	// stopien wielomianu,  a,   b,  x1,   wartosci przy x dla wielomianu

	string x0;
	string b;
	string y0;
	string h;

	getline(loadFile, x0, ',');
	getline(loadFile, b, ',');
	getline(loadFile, y0, ',');
	getline(loadFile, h, ',');

	double* tab = new double[4];

	tab[0] = stod(x0);
	tab[1] = stod(b);
	tab[2] = stod(y0);
	tab[3] = stod(h);

	cout << "x0:  " << tab[0] << endl;
	cout << "b:  " << tab[1] << endl;
	cout << "y0:  " << tab[2] << endl;
	cout << "h:  " << tab[3] << endl;

	cout << endl << "-------------------------------------------------------------------------------------" << endl << endl;

	loadFile.close();

	return tab;
}

double licz(double x, double y) {
	return 4 * cos(x) - 6 * sin(x) - 10 * y;
}