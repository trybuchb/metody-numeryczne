#include<iostream>
#include<fstream>
#include<string>

using namespace std;

string fileName = "load.csv";
double* load();
void Euler(double x0, double b, double y0, double h);


int main() {

	double* tab2 = load();

	Euler(tab2[0], tab2[1], tab2[2], tab2[3]);

	system("pause");
	return 0;
}

double* load() {

	ifstream loadFile;
	loadFile.open(fileName);

	if (!loadFile.is_open()) {
		cout << "Error loading" << endl;
		return false;
	};

	cout << "Wczytane watrosci: " << endl;

	//        [0]          [1]  [2] [3]           [4+]
	// stopien wielomianu,  a,   b,  x1,   wartosci przy x dla wielomianu

	string x0;
	string b;
	string y0;
	string h;

	getline(loadFile, x0, ',');
	getline(loadFile, b, ',');
	getline(loadFile, y0, ',');
	getline(loadFile, h, ',');

	double* tab = new double[4];

	tab[0] = stod(x0);
	tab[1] = stod(b);
	tab[2] = stod(y0);
	tab[3] = stod(h);

	cout << "x0:  " << tab[0] << endl;
	cout << "b:  " << tab[1] << endl;
	cout << "y0:  " << tab[2] << endl;
	cout << "h:  " << tab[3] << endl;

	cout << endl << "-------------------------------------------------------------------------------------" << endl << endl;

	loadFile.close();

	return tab;
}


void Euler(double x0, double b, double y0, double h)
{
	double N = (b - x0) / h;
	double**tab = new double*[N];
	for (int i = 0; i < N; ++i)
	{
		tab[i] = new double[2];
	}


	double y = y0, x = x0;
	for (int i = 0; i < N; ++i)
	{
		for (int j = 0; j<2; ++j)
		{
			if (j == 0)
			{
				tab[i][j] = x + h;
				x = tab[i][j];
			}
			else
			{
				tab[i][j] = y + h * y;
				y = tab[i][j];
			}
		}
	}

	for (int i = 0; i < N; ++i)
	{
		cout << tab[i][0] << " " << tab[i][1] << endl;
	}
}