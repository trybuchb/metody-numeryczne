#include <iostream>
#include <string>
#include <fstream>

using namespace std;

string fileName = "load.csv";

bool load(int iloscWartosci, double** wartosci);

int main() {

	int iloscWartosci;

	cout << "Ilosc wartosci: ";
	cin >> iloscWartosci;

	double** wartosci = new double*[iloscWartosci];
	double* l = new double[iloscWartosci];
	double punkt;

	for (int i = 0; i < iloscWartosci; ++i)
		wartosci[i] = new double[2];

	load(iloscWartosci, wartosci);

	cout << "Wyznacz dla punktu: ";
	cin >> punkt;

	for (size_t i = 0; i < iloscWartosci; i++)
	{
		l[i] = 1;
		for (size_t j = 0; j < iloscWartosci; j++)
		{
			if (j!=i)
			{
				l[i] *= (punkt - wartosci[j][0]) / (wartosci[i][0] - wartosci[j][0]);
			}
		}
			
	}

	double wynik = 0;

	for (size_t i = 0; i < iloscWartosci; i++)
	{
		wynik += (wartosci[i][1] * l[i]);
	}

	cout << "Wynik: " <<wynik << endl;


	system("PAUSE");
}

bool load(int iloscWartosci, double** wartosci) {

	ifstream loadFile;
	loadFile.open(fileName);

	if (!loadFile.is_open()) {
		cout << "Error loading" << endl;
		return false;
	};

	cout << "Wczytane watrosci: " << endl;
	for (size_t i = 0; i < iloscWartosci; i++)
	{
		string x;
		string y;

		getline(loadFile, x, ',');
		getline(loadFile, y, '\n');

		wartosci[i][0] = stod(x);
		wartosci[i][1] = stod(y);

		cout << x << " , " << y << endl;

	}

	cout << endl << "-------------------------------------------------------------------------------------" << endl;

	loadFile.close();
	return wartosci;
}