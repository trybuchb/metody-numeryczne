#include <iostream>
#include <string>
#include <fstream>
#include <ctime>
#include <cstdlib>

using namespace std;

string fileName = "load.csv";

double** load();

int main() {

	double** punkty = new double*[4];
	double waga[2];
	double punkt[2];
	double fksztalt[2][2][4];
	double po_ksi[2][4];
	double po_ni[2][4];
	double fun_detj[2][2];


	for (size_t i = 0; i < 4; i++)
	{
		punkty[i] = new double[2];
	}

	punkty = load();

	double wsp_x[4] = { punkty[0][0], punkty[1][0], punkty[2][0], punkty[3][0] };
	double wsp_y[4] = { punkty[0][1], punkty[1][1], punkty[2][1], punkty[3][1] };

	waga[0] = 1;
	waga[1] = 1;

	punkt[0] = -0.5773502692;
	punkt[1] = 0.5773502692;

	double pole = 0;
	double ShapeF = 0;
	double wsp = 0;

	for (size_t ie = 0; ie < 4; ie++)
	{
		for (size_t je = 0; je < 4; je++)
		{
			for (size_t jpn = 0; jpn < 2; jpn++)
			{
				for (size_t ipn = 0; ipn < 2; ipn++)
				{
					fksztalt[ipn][jpn][0] = 0.25 * (1 - punkt[ipn]) * (1 - punkt[jpn]);
					fksztalt[ipn][jpn][1] = 0.25 * (1 + punkt[ipn]) * (1 - punkt[jpn]);
					fksztalt[ipn][jpn][2] = 0.25 * (1 + punkt[ipn]) * (1 + punkt[jpn]);
					fksztalt[ipn][jpn][3] = 0.25 * (1 - punkt[ipn]) * (1 + punkt[jpn]);

					po_ni[jpn][0] = -0.25 * (1 - punkt[jpn]);
					po_ni[jpn][1] =  0.25 * (1 - punkt[jpn]);
					po_ni[jpn][2] =  0.25 * (1 + punkt[jpn]);
					po_ni[jpn][3] = -0.25 * (1 + punkt[jpn]);

					po_ksi[ipn][0] = -0.25 * (1 - punkt[ipn]);
					po_ksi[ipn][1] = -0.25 * (1 + punkt[ipn]);
					po_ksi[ipn][2] =  0.25 * (1 + punkt[ipn]);
					po_ksi[ipn][3] =  0.25 * (1 - punkt[ipn]);

					double FdXdQ = po_ni[jpn][0] * wsp_x[0] + po_ni[jpn][1] * wsp_x[1] + po_ni[jpn][2] * wsp_x[2] + po_ni[jpn][3] * wsp_x[3];
					double FdYdQ = po_ni[jpn][0] * wsp_y[0] + po_ni[jpn][1] * wsp_y[1] + po_ni[jpn][2] * wsp_y[2] + po_ni[jpn][3] * wsp_y[3];

					double FdXdE = po_ksi[jpn][0] * wsp_x[0] + po_ksi[jpn][1] * wsp_x[1] + po_ksi[jpn][2] * wsp_x[2] + po_ksi[jpn][3] * wsp_x[3];
					double FdYdE = po_ksi[jpn][0] * wsp_y[0] + po_ksi[jpn][1] * wsp_y[1] + po_ksi[jpn][2] * wsp_y[2] + po_ksi[jpn][3] * wsp_y[3];

					fun_detj[ipn][jpn] = FdXdQ * FdYdE - FdXdE * FdYdQ;

					wsp = abs(fun_detj[ipn][jpn]) * waga[ipn] * waga[jpn];

					ShapeF = fksztalt[ipn][jpn][ie];

				}
			}
			pole = pole + wsp * ShapeF;
		}
	}

	cout << "Pole wynosi: " << pole << endl << endl;

	system("PAUSE");
}

double** load() {

	ifstream loadFile;
	loadFile.open(fileName);

	if (!loadFile.is_open()) {
		cout << "Error loading" << endl;
		return false;
	};

	cout << "Wczytane watrosci: " << endl << "-------------------------------------------------------------------------------------" << endl << endl;

	double** punkty = new double*[4];

	for (size_t i = 0; i < 4; i++)
	{
		punkty[i] = new double[2];
	}

	for (size_t i = 0; i < 4; i++)
	{
		string z;
		getline(loadFile, z, ',');
		punkty[i][0] = stod(z);
		cout << i << " X:  " << punkty[i][0] << endl;

		getline(loadFile, z, ',');
		punkty[i][1] = stod(z);
		cout << i << " Y:  " << punkty[i][1] << endl << endl;
	}



	cout << "-------------------------------------------------------------------------------------" << endl << endl;

	loadFile.close();

	return punkty;
}