#include <iostream>
#include <string>
#include <fstream>

using namespace std;
string fileName = "load.csv";

double* liczWyznaczniki(double* m[]);
double* liczXYZ(double m[]);
void pokaz(double* m[]);
bool load(int iloscWartosci, double** wartosci);

int main() {
	/*
	double** m = new double*[3];

	for (size_t i = 0; i < 4; i++)
	{
		m[i] = new double[4];
	}

	m[0][0] = 2;
	m[0][1] = 5;
	m[0][2] = 3;
	m[0][3] = 5;

	m[1][0] = 4;
	m[1][1] = 2;
	m[1][2] = 5;
	m[1][3] = 4;

	m[2][0] = 3;
	m[2][1] = 8;
	m[2][2] = 4;
	m[2][3] = 9;
	

	pokaz(m);

	double* wyznaczniki = new double[4];
	wyznaczniki = liczWyznaczniki(m);

	double* wyniki = new double[3];
	wyniki = liczXYZ(wyznaczniki);

	cout << "y = " << wyniki[0] << "x^2 + " << wyniki[1] << "x + " << wyniki[2] << endl;
	cout << "----------------------------------------------------------------------------------" << endl;
	*/
//-------------------------------------------------------------------------------------------------------------
	
	double** wartosci = new double*[3];
	for (int i = 0; i < 3; ++i)
		wartosci[i] = new double[2];
	//double** wartosci[3][2];

	//wartosci[0][0] = 2;
	//wartosci[0][1] = 0;

	//wartosci[1][0] = 3;
	//wartosci[1][1] = 2;

	//wartosci[2][0] = 5;
	//wartosci[2][1] = 4;

	load(3, wartosci);

	double** m2 = new double*[3];

	for (size_t i = 0; i < 4; i++)
	{
		m2[i] = new double[4];
	}

	m2[0][0] = wartosci[0][0] * wartosci[0][0];
	m2[0][1] = wartosci[0][0];
	m2[0][2] = 1;
	m2[0][3] = wartosci[0][1];

	m2[1][0] = wartosci[1][0] * wartosci[1][0];
	m2[1][1] = wartosci[1][0];
	m2[1][2] = 1;
	m2[1][3] = wartosci[1][1];

	m2[2][0] = wartosci[2][0] * wartosci[2][0];
	m2[2][1] = wartosci[2][0];
	m2[2][2] = 1;
	m2[2][3] = wartosci[2][1];

	pokaz(m2);

	double* wyznaczniki2 = new double[4];
	wyznaczniki2 = liczWyznaczniki(m2);

	double* wyniki2 = new double[3];
	wyniki2 = liczXYZ(wyznaczniki2);

	cout << "y = " << wyniki2[0] << "x^2 + " << wyniki2[1] << "x + " << wyniki2[2] << endl;
	
	system("PAUSE");
}

double* liczWyznaczniki(double* m[]) {

	double* wyznaczniki = new double[4];

	wyznaczniki[0] = (m[0][0] * m[1][1] * m[2][2]) + (m[0][1] * m[1][2] * m[2][0]) + (m[0][2] * m[1][0] * m[2][1])
		- (m[0][2] * m[1][1] * m[2][0]) - (m[0][1] * m[1][0] * m[2][2]) - (m[0][0] * m[1][2] * m[2][1]);

	wyznaczniki[1] = (m[0][3] * m[1][1] * m[2][2]) + (m[0][1] * m[1][2] * m[2][3]) + (m[0][2] * m[1][3] * m[2][1])
		- (m[0][2] * m[1][1] * m[2][3]) - (m[0][1] * m[1][3] * m[2][2]) - (m[0][3] * m[1][2] * m[2][1]);

	wyznaczniki[2] = (m[0][0] * m[1][3] * m[2][2]) + (m[0][3] * m[1][2] * m[2][0]) + (m[0][2] * m[1][0] * m[2][3])
		- (m[0][2] * m[1][3] * m[2][0]) - (m[0][3] * m[1][0] * m[2][2]) - (m[0][0] * m[1][2] * m[2][3]);

	wyznaczniki[3] = (m[0][0] * m[1][1] * m[2][3]) + (m[0][1] * m[1][3] * m[2][0]) + (m[0][3] * m[1][0] * m[2][1])
		- (m[0][3] * m[1][1] * m[2][0]) - (m[0][1] * m[1][0] * m[2][3]) - (m[0][0] * m[1][3] * m[2][1]);

	cout << "Wyznacznik: " << wyznaczniki[0] << endl;
	cout << "Wyznacznik X: " << wyznaczniki[1] << endl;
	cout << "Wyznacznik Y: " << wyznaczniki[2] << endl;
	cout << "Wyznacznik Z: " << wyznaczniki[3] << endl;

	return wyznaczniki;
}
double* liczXYZ(double m[]) {

	double W = m[0];
	double Wx = m[1];
	double Wy = m[2];
	double Wz = m[3];

	if (W == 0)
	{
		if (Wx != 0 || Wy != 0 || Wz != 00)
		{
			cout << "Uklad sprzeczny." << endl;
			system("PAUSE");
			return 0;
		}
		else if (Wx == 0 && Wy == 0 && Wz == 0)
		{
			cout << "Uklad nieoznaczony." << endl;
			system("PAUSE");
			return 0;
		}
	}
	
	double x, y, z;

	x = Wx / W;
	y = Wy / W;
	z = Wz / W;

	cout << "X: " << x << endl;
	cout << "Y: " << y << endl;
	cout << "Z: " << z << endl;

	cout << "----------------------------------------------------------------------------------" << endl;

	double* wyniki = new double[3];

	wyniki[0] = x;
	wyniki[1] = y;
	wyniki[2] = z;

	return wyniki;
}
void pokaz(double* m[]) {
	for (size_t i = 0; i < 3; i++)
	{
		for (size_t j = 0; j < 4; j++)
		{
			cout << m[i][j] << " ";
			if (j == 2)
			{
				cout << "= ";
			}
		}
		cout << endl;
	}

}

bool load(int iloscWartosci, double** wartosci) {

	ifstream loadFile;
	loadFile.open(fileName);

	if (!loadFile.is_open()) {
		cout << "Error loading" << endl;
		return false;
	};

	cout << "Wczytane watrosci: " << endl;
	for (size_t i = 0; i < 3; i++)
	{
		string x;
		string y;

		getline(loadFile, x, ',');
		getline(loadFile, y, '\n');

		wartosci[i][0] = stod(x);
		wartosci[i][1] = stod(y);

		cout << x << " , " << y << endl;

	}

	cout << endl << "-------------------------------------------------------------------------------------" << endl;

	loadFile.close();
	return wartosci;
}