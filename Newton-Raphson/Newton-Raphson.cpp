#include <iostream>
#include <string>
#include <fstream>

using namespace std;

string fileName = "load.csv";

double* load();
double liczWielomian(double* tab, double x);

int main() {
	double* tab = load();
	double* tab2 = new double[7];  //pochodna
	tab2[0] = 2; 
	tab2[4] = 0;
	tab2[5] = 10;
	tab2[6] = 6;

	double wynik = 0;
	double a = tab[1];
	double b = tab[2];

	double x1 = a;
	double x2;

	
	while (true)
	{
		x2 = x1 - (liczWielomian(tab, x1) / liczWielomian(tab2, x1));
		//cout << x2 << endl;

		if (abs(x2 - x1) < 0.0000001)
		{
			wynik = x2;
			break;
		}

		x1 = x2;
	}
	
	cout << "Wynik: " << wynik << endl;

	system("PAUSE");
}

double* load() {

	ifstream loadFile;
	loadFile.open(fileName);

	if (!loadFile.is_open()) {
		cout << "Error loading" << endl;
		return false;
	};

	cout << "Wczytane watrosci: " << endl;

	//        [0]          [1]  [2] [3]           [4+]
	// stopien wielomianu,  a,   b,  x1,   wartosci przy x dla wielomianu

	string stopien;
	string a;
	string b;
	string x;

	getline(loadFile, stopien, ',');

	int st = stoi(stopien);
	double* tab = new double[st + 4];

	getline(loadFile, a, ',');
	getline(loadFile, b, ',');
	getline(loadFile, x, ',');

	tab[0] = stod(stopien);
	tab[1] = stod(a);
	tab[2] = stod(b);
	tab[3] = stod(x);

	string temp;
	for (size_t i = 4; i < st + 4; i++)
	{
		getline(loadFile, temp, ',');
		tab[i] = stod(temp);
	}

	getline(loadFile, temp, '\n');
	tab[st + 4] = stod(temp);

	cout << "Stopien:  " << tab[0] << endl;
	cout << "A:  " << tab[1] << endl;
	cout << "B:  " << tab[2] << endl;
	cout << "X:  " << tab[3] << endl;

	for (size_t i = 4; i < st + 5; i++)
	{
		cout << "X^" << i - 4 << ":  " << tab[i] << endl;
	}



	cout << endl << "-------------------------------------------------------------------------------------" << endl << endl;

	loadFile.close();

	return tab;
}

double liczWielomian(double* tab, double x) {

	double wynik = 0;

	for (size_t i = 0; i <= tab[0]; i++)
	{
		wynik += tab[i + 4] * pow(x, i);
	}

	return wynik;
}