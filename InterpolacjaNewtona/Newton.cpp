#include <iostream>
#include <string>
#include <fstream>

using namespace std;

string fileName = "load.csv";

bool load(int iloscWartosci, double** wartosci);

int main() {

	int iloscWartosci;
	cout << "Ilosc wartosci: ";
	cin >> iloscWartosci;
	
	double** rzedy = new double*[iloscWartosci-1];
	int gh = 0;
	for (int i = iloscWartosci - 1; i >= 1; i--)
	{
		rzedy[gh] = new double[iloscWartosci];
		gh++;
	}

	double** wartosci = new double*[iloscWartosci];
	for (int i = 0; i < iloscWartosci; ++i)
		wartosci[i] = new double[2];
	
	load(iloscWartosci, wartosci);

	int ileWielomianowWrzedzie = iloscWartosci - 1;
	int ileXwNawiasie = 2;

	//jedziemy po rzedach
	for (size_t i = 0; i < iloscWartosci - 1; i++)
		// i = rzad
	{

		int p = ileXwNawiasie - 1;

		for (size_t j = 0; j < ileWielomianowWrzedzie; j++) 
			// j = indeks wartosc wielomianu w rzedzie
		{

			if (i == 0)
			{
				rzedy[i][j] = (wartosci[j + 1][1] - wartosci[j][1]) / (wartosci[j + 1][0] - wartosci[j][0]);
			}
			else
			{

				for (size_t l = 0; l < ileWielomianowWrzedzie; l++)
				{
					rzedy[i][j] = (rzedy[i - 1][j + 1] - rzedy[i - 1][j]) / (wartosci[p][0] - wartosci[p - ileXwNawiasie + 1][0]);				
				}
				p++;
			}
			
		}
		ileWielomianowWrzedzie--;
		ileXwNawiasie++;
	}

	int ileWKolumnie = iloscWartosci - 1;

	for (size_t i = 0; i < iloscWartosci - 1; i++)
	{
		cout << "Rzad: " << i + 1 << endl;

		for (size_t j = 0; j < ileWKolumnie; j++)
		{
			cout <<rzedy[i][j] << endl;
		}
		cout << "-------------------------------------------------------------------------------------" << endl;
		ileWKolumnie--;
	}

	double x;

	cout << "Oblicz dla X: ";
	cin >> x;

	double y = wartosci[0][1];

	for (size_t i = 0; i < iloscWartosci-1; i++)
	{
		double z = rzedy[i][0];
		for (size_t j = 0; j < i+1; j++)
		{
			z *=(x - wartosci[j][0]);
		}
		y += z;
	}

	cout << "Wynik: " << y << endl;


	system("PAUSE");
}

bool load(int iloscWartosci, double** wartosci) {

	ifstream loadFile;
	loadFile.open(fileName);

	if (!loadFile.is_open()) {
		cout << "Error loading" << endl;
		return false;
	};

	cout << "Wczytane watrosci: " << endl;
	for (size_t i = 0; i < iloscWartosci; i++)
	{
		string x;
		string y;

		getline(loadFile, x, ',');
		getline(loadFile, y, '\n');

		wartosci[i][0] = stod(x);
		wartosci[i][1] = stod(y);

		cout << x << " , " << y << endl;
		
	}

	cout << endl << "-------------------------------------------------------------------------------------" << endl;

	loadFile.close();
	return wartosci;
}